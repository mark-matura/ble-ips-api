# BLE IPS Backend
This program processes measurements coming in from the BLE IPS Client. It abstracts the calculations and provides an interface to get the positions of devices via a RESTful API. As long as the JSON format contains all necessary information, the BLE packet telemetry of any device can be ingested and processed in real time. As this API provides a very general data interface, it is easy to integrate this in IoT applications.

> [Database Schema](https://dbdiagram.io/embed/5e887d7d4495b02c3b893e6c)

## Hardware Requirements
All x86_64 and arm32v7 based devices that can run docker are guaranteed to work. Older arm32v6 based boards such as the raspberry pi zero w are not supported.

Devices I tested on are:
- Raspbian Stretch on arm32v7 Raspberry Pi 3 Model B
- Raspbian Buster on arm32v7 Raspberry Pi 4 Model B 1GB and 4GB
- Debian Buster on AMD64 based server
- Native Windows on AMD64 based gaming PC
- WSL (bare metal) on AMD64 based gaming PC
- Manjaro on Intel x86_64 based Laptop

## Software Installation
The BLE IPS API is packaged as a docker image and can be installed + configured with very little effort. The only prerequisite is that you have Docker installed on your host machine.

### Install Docker and Docker Compose
#### Windows 10
Visit the [Docker Downloads page](https://docs.docker.com/get-docker/) to install the software for Windows 10. Make sure Hyper-V is enabled in Windows features and in BIOS.

#### MacOS
```sh
# install
brew install docker
```
#### Raspbian
Follow [this](https://dev.to/rohansawant/installing-docker-and-docker-compose-on-the-raspberry-pi-in-5-simple-steps-3mgl) tutorial

### Clone the folder
```sh
# Clone the repository from GitLab using
git clone https://gitlab.com/mark-matura/ble-ips-api

cd ble-ips-api
cp .env.example .env

# set the database password to something more secure
sudo nano .env
```

### Install docker images and start services
```sh
# set correct file and folder permission on PWD for docker and nginx
chmod -R 755 .

# Build the API container using docker compose
docker-compose build

# Enable the services
docker-compose up -d

# Get bash window to interact with the docker container
docker-compose exec api bash
```
Inside the shell prompt enter:
```sh
# install all composer packages
$ composer install

# create MySQL tables and enter sample data
$ php artisan migrate:fresh --seed
```

### Helpful Docker Compose Commands
```sh
# list active services
docker-compose ps

# pause services
docker-compose pause

# resume services
docker-compose unpause

# Shut down containers, networks and volumes
docker-compose down
```

## Monitor Cache and RDBMS
Docker Compose automatically starts some services that are for monitoring the state of the system. There also is a PhpMyAdmin Container on port `3000`. The PMA password is defined in the .env file, the username is root.

Asssuming your services are trying to access the active services running locally on your host machine, enter [http://localhost:3000](http://localhost:3000)respectively to access them.

## Alternative bare metal server setup (tedious, untested since 29.06.20)
If for whatever reason you cannot install docker, you can use a bare metal setup instead. Performance might be marginally better.

This configuration worked used PHP 7.4.6 on Ubuntu 18.04 LTS and Debian 10. If you are on Windows install WSL since it is easier to install and configure the server.

Important: for this option it is necessary to change the .env flie to update the hostnames for all services to localhost or whatever you are using. The current options use hostnames that are resolved to the docker containers inside of docker itself.

### Redis
This Backend uses Redis to persist data in memory across PHP requests.
```sh
# install
sudo apt-get update
sudo apt-get install redis-server

# then disable background saving which causes read errors by commenting out lines with save prefix
sudo nano /etc/redis/redis.conf
```

### Apache
Install php fpm because redis reuses peristent connections to reduce load.

```sh
sudo apt install php-fpm

# add line extension=redis.so to php.ini config file
sudo nano /etc/php/7.4/fpm/php.ini

# .htaccess
sudo a2enmod rewrite
sudo nano /etc/apache2/sites-available/yourfolder.conf
```
Paste this in yourfolder.conf
```
# turn on rewrite engine and allow override by .htaccess in public folder
# Here is a sample config for pure http sockets, use certbot for https
<VirtualHost *:80>
    ServerName localhost # or just the domain name you are using
    DocumentRoot /yourwebroot/yourfolder/public
    RewriteEngine on

    <Directory /yourwebroot/yourfolder>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>

    <FilesMatch ".php$">
        SetHandler "proxy:unix:/var/run/php/php7.4-fpm.sock|fcgi://localhost/"
    </FilesMatch>

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

Duplicate and rename the `.env.example` file to `.env`. Afterwards be sure to change the `DB_HOST` and `REDIS_HOST` to the localhost `127.0.0.1` or whatever you are using.

### Install composer packages and migrate the database
``` bash
# install composer packages
composer install

# migrate the database
php artisan migrate

# seed the database tables
php artisan db:seed

# run php server
php -S localhost:8000 -t public

# to reset the database and seed all tables run :
php artisan migrate:refresh --seed
```
By [Mark Marolf](https://markmarolf.com/)
