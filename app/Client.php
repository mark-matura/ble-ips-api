<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'name',
        'mac_addr',
        'actual_x',
        'actual_y',
        'color',
    ];

    /**
     * The attributes that should be cast to native types.
     * https://laravel.com/docs/6.x/eloquent-mutators#attribute-casting
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'mac_addr' => 'string',
        'actual_x' => 'float',
        'actual_y' => 'float',
        'color' => 'string',
    ];

    /**
     * Get the last 10 positions for the client
     */
    public function positions()
    {
        return $this->hasMany('App\Position')->latest()->take(10);
    }
}
