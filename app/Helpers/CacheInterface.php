<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Redis;
use Log;

class CacheInterface {
    // Notice: Static methods are acceptable here because no internal state of the class is modified.
    // This class really is just an adapter for the IPS to access the Cache

    /**
     * @param string $key The name of the list with measurememts
     * @param measurement string or object containing the measurement that needs to be persisted
     *
     * @return void
     */
    public static function storeMeasurement(string $key, $measurement): void {
        Redis::lpush($key, json_encode($measurement));

        if ($measurement['origin'] !== 'calibration') {
            Redis::ltrim($key, 0, 999);
        }
    }

    /**
     * @param string $key The name of the list with measurememts
     * @param int $amount (optional) How many measurements should be retrieved from the backend, empty = all
     *
     * @return void
     */
    public static function retrieveMeasurements(string $key, int $amount = 0): array {
        log::info('getting latest '.$amount.' '.$key.' measurements');
        $measurements = Redis::lrange($key, 0, ($amount - 1));
        log::info('got '.count($measurements).' measurements');

        $measurements = array_map( function($measurement) { return json_decode($measurement);}, $measurements);
        return $measurements;
    }

    public static function deleteKey(string $key): void {
        Redis::del($key);
    }

    public static function flushAll(): void {
        Redis::flushdb();
    }
}