<?php

namespace App\Helpers;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

use Log;

class CalculationHelper {
    public static function getIndexOfMeasurement(object $measurement, array $measurements): int {
        if (!count($measurements)) {
            throw new \Exception('No measurements provided to index retriever');
        }

        log::info('getting the index of a measurement with origin: '.$measurement->origin.'in '.count($measurements).' measurements');
        log::info('in_array ($measurement, $measurements)');
        log::info(in_array ($measurement, $measurements));
        log::info(':::::::::::::::::::::::::::');
        $index = array_search($measurement, $measurements);

        if ($index) {
            log::info('position of measurement in pool: '.$index.' of '.count($measurements));
            return $index;
        }

        // fallback to searching
        throw new \Exception ('Could not find index');
    }

    public static function getLogScaledAverageRssi(array $measurements): float {
        if (count($measurements) == 0) {
            throw new \Exception('Supplied log scaling function with an empty measurement array');
        }
        $rssiValues = array_column($measurements, 'rssi');

        $rssiConvertedToMw = array_map(function($rssiInDBm) {
            return pow(10, $rssiInDBm / 10);
        }, $rssiValues);

        $averageInMw = array_sum($rssiConvertedToMw) / count($measurements);
        $averageConvertedBackToDbm = 10 * \log10($averageInMw);

        return $averageConvertedBackToDbm;
    }

    public static function getDistance(int $rssi) {
        // (rssi + a_0) / -10 * n
        $exponent = ($rssi - 47.899677614236) / (-10 * 2.2421283703413);
        return pow(10, $exponent);
    }
}
?>
