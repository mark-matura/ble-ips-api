<?php

namespace App\Helpers;

use Log;

class FindMostRecentMeasurement {
    private $measurementsToSearch = array();
    private $targetMeasurement;
    private $mostRecent;

    /**
     * @param object $targetMeasurement The measurement for which the most recent counterpart should be searched
     * @param array $measurementsToSearch Array of measurements in which the most recent relative to target should be searched
     *
     * @return void
     */
    public function __construct(object $targetMeasurement, array $measurementsToSearch) {
        if(!count($measurementsToSearch)) {
            throw new \Exception('No measurements, cannot find most recent');
        }

        $this->targetMeasurement = $targetMeasurement;
        $this->measurementsToSearch = (array) $measurementsToSearch;

        $this->findMostRecentMeasurement();
    }

    private function findMostRecentMeasurement(): void {
        $lowestTimeDifference = PHP_INT_MAX;

        foreach ($this->measurementsToSearch as $measurement) {
            $measurement = (array) $measurement;
            $timeDelta = abs($measurement['timestamp'] - $this->targetMeasurement->timestamp);

            if ($timeDelta < $lowestTimeDifference) {
                $this->mostRecent = $measurement;
                $lowestTimeDifference = $timeDelta;
            }
        }

        Log::info('lowest delta for '.$this->targetMeasurement->origin.': '.$lowestTimeDifference.' ms');
        Log::info($this->targetMeasurement->origin."\t".$this->targetMeasurement->timestamp."\t".$this->mostRecent['origin']."\t".$this->mostRecent['timestamp']);
    }

    /**
     * get the most recent measurement relative to target
     *
     * @return object $this->mostRecent get the most recent measurement relative to target
     */
    public function get(): object {
        return (object) $this->mostRecent;
    }
}
?>
