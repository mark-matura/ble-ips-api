<?php

namespace App\Helpers;
use Log;

/**
 * @brief Do log linear regression on measurements using given a in f(x) = -10n log(d) + a
 *
 * return n coefficient of type float
 * @see https://mathworld.wolfram.com/LeastSquaresFittingLogarithmic.html
 */
class FitCalibrationData {
    private $distances = [];
    private $rssi = [];
    private $measurementCount = 0;
    private $logScaledDistance = [];
    private $sumXyProducts = 0;
    private $x_squared_sum = 0;

    private $nFit = 0;
    private $aFit = 0;

    public function __construct($measurements) {
        $this->validateInput($measurements);
        $this->distances = array_column($measurements, 'distance');
        $this->rssi = $rssi = array_column($measurements, 'rssi');
        $this->measurementCount = count($measurements);

        $this->coordinateFit();
    }

    private function validateInput($measurements) {
        if (count($measurements)  === 0) {
            throw new \Exception('No distances supplied to calibration data fit implementation');
        }
    }

    private function coordinateFit() {
        try {
            $this->logScaleTheDistancesForLinearRegression();
            $this->getProductAndSquareOfAllXY();
            $this->fitPathLossParameters();
        } catch (\Exception $e) {
            Log::error($e);
        }
    }

    private function logScaleTheDistancesForLinearRegression() {
        $this->logScaledDistance = array_map('log10', $this->distances);
    }

    private function getProductAndSquareOfAllXY() {
        $this->sumXyProducts = array_sum(
            array_map(function($x, &$y) {return $x*$y;}, $this->logScaledDistance, $this->rssi)
        );

        $this->x_squared_sum = array_sum(array_map(function($x) {return pow($x,2);}, $this->logScaledDistance));

        log::info('$sumXyProducts '.$this->sumXyProducts);
        log::info('$x_squared_sum '.$this->x_squared_sum);
    }

    private function fitPathLossParameters() {
        log::info('back part of denominator '.pow(array_sum($this->logScaledDistance), 2));

        $this->nFit = ($this->measurementCount * $this->sumXyProducts - array_sum($this->rssi) * array_sum($this->logScaledDistance)) /
        ($this->measurementCount * $this->x_squared_sum - pow(array_sum($this->logScaledDistance), 2));

        $this->aFit = (array_sum($this->rssi) - $this->nFit * array_sum($this->logScaledDistance)) / $this->measurementCount;

        // IMPORTANT: this has to be AFTER calculating the N fit, otherwise results are wrong for the A fit
        $this->nFit = $this->nFit / -10;
    }

    public function getNFit() {
        return $this->nFit;
    }

    public function getAFit() {
        return $this->aFit;
    }
}
?>
