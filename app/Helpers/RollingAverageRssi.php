<?php
namespace App\Helpers;
use App\Helpers\CalculationHelper;
use Log;

class RollingAverageRssi {
    private $measurement;
    private $measurements = array();

    private $windowSize = 10;

    public function __construct(object $measurement, array $measurements) {
        if (count($measurements) === 0) {
            throw new \Exception('No measurements supplied to RollingAverageRssi');
        }
        $this->measurement = (object) $measurement;
        $this->measurements = $measurements;

        $this->calculateRollingAverage();
    }

    // returns the average between the measurement and 5 measurements before and after
    private function calculateRollingAverage() {
        $index = CalculationHelper::getIndexOfMeasurement($this->measurement, $this->measurements);
        $start =  ($index - ($this->windowSize / 2));
        $measurementsInWindow = array_slice($this->measurements, $start, ($this->windowSize + 1));

        log::info('RSSI values found: '.count($measurementsInWindow));

        $this->measurement->rssi = CalculationHelper::getLogScaledAverageRssi($measurementsInWindow);
    }

    public function getResult() {
        return $this->measurement->rssi;
    }
}
?>
