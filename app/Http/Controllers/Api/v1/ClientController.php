<?php

namespace App\Http\Controllers\Api\v1;
use Illuminate\Http\Request;

use App\Helpers\CalculationHelper;
use App\Helpers\CacheInterface;
use App\Client;
use App\Server;

use Log;

use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\DB;

class ClientController extends Controller
{
    public function getAll(Request $request) {
        return Client::with('positions')->get();
    }

    public function getById(Request $request) {
        $this->validate($request, [
            'id' => 'required|integer|exists:App\Client,id',
        ]);

        return Client::where('id', $request->id)->first();
    }

    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required|string',
            'mac_addr' => 'required|string',
        ]);

        if (!Client::where("mac_addr", $request->mac_addr)->exists()) {
            $client = new Client;
            $client->name = $request->name;
            $client->mac_addr = $request->mac_addr;
            $client->save();
            return response('Successfully saved the client', 200);
        }
        return response('Client already exists', 200);
    }

    public function update(Request $request) {
        $this->validate($request, [
            'id' => 'required|integer|exists:App\Client,id',
            'name' => 'required|string|max:100',
            'mac_addr' => 'required|string|unique:App\Server,mac_addr',
            'actual_x' => 'required',
            'actual_y' => 'required',
            'color' => 'required|string',
        ]);

        $client = Client::where('id', $request->id)->first();
        $client->name = $request->name;
        $client->mac_addr = $request->mac_addr;
        $client->actual_x = $request->actual_x;
        $client->actual_y = $request->actual_y;
        $client->color = $request->color;
        $client->save();

        return Response('Successfully updated the client with MAC address "'.$client->mac_addr.'"', 200);
    }

    public function remove(Request $request) {
        $this->validate($request, [
            'id' => 'required|integer|exists:App\Client,id',
        ]);

        $client = $this->getById($request);
        $client->delete();

        return Response('Successfully removed the client with MAC address "'.$client->mac_addr.'"', 200);
    }
}
