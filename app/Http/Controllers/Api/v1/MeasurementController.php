<?php

namespace App\Http\Controllers\Api\v1;

use App\Helpers\CacheInterface;

use Illuminate\Support\Facades\Redis;
use Illuminate\Http\Request;

use Log;

class MeasurementController extends Controller
{
    /**
    * @param Request $request
    *
    * get all calibration measurements
    *
    * @return \Illuminate\Http\Response
    */
    public function getCalibrationData(Request $request) {
        $measurements = CacheInterface::retrieveMeasurements('calibration');
        return $this->tabSeparatedMeasurements($measurements);
    }

    private function tabSeparatedMeasurements($measurements) {
        $tabSeparatedMeasurements = "";

        foreach ($measurements as $measurement) {
            $constructedLine = implode("\t", $measurement).'\n';
            $tabSeparatedMeasurements = $tabSeparatedMeasurements.$constructedLine;
        }

        return $tabSeparatedMeasurements;
    }

    public function getForServerAndClient(Request $request) {
        $this->validate($request, [
            'server_addr' => 'required|string',
            'client_addr' => 'required|string',
        ]);

        $key = 'server:'.$request->server_addr.':client:'.$request->client_addr.':measurements';

        return CacheInterface::retrieveMeasurements($key, 50);
    }

    /**
    * @param Request $request
    *
    * store a new measurement
    *
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request): \Illuminate\Http\Response {
        $this->validate($request, [
            'client_addr' => 'required|string',
            'measurements' => 'required',
        ]);

        // save all measurements
        foreach ($request->measurements as $measurement) {
            if (!isset($measurement['origin'])) {
                return abort(500, 'Measurement origin is not set');
            }
            $this->routeMeasurementToStores($measurement, $request->client_addr);
        }
        return response('Successfully stored the measurements', 200);
    }

    private function routeMeasurementToStores($measurement, $client_addr) {
        switch ($measurement['origin']) {
            case 'gateway':
                log::info("rawgateway\t".$measurement['timestamp']."\t".$measurement['rssi']);
                $this->storeGatewayMeasurement($measurement);
                break;
            case 'client':
                // event(new IncomingClientMeasurement($measurement, $client_addr));

                $key = 'server:'.$measurement['server_addr'].':client:'.$client_addr.':measurements';
                CacheInterface::storeMeasurement($key, $measurement);

                log::info("rawclient\t".$measurement['timestamp']."\t".$measurement['rssi']);
                break;
            case 'calibration':
                $this->storeCalibrationMeasurement($measurement);
                break;
            default:
                throw new \Exception('Unknown origin');
        }
    }

    public function storeGatewayMeasurement($measurement) {
        $key = 'gateway:server:'.$measurement['server_addr'];
        CacheInterface::storeMeasurement($key, $measurement);
    }

    public function storeCalibrationMeasurement($measurement) {
        CacheInterface::storeMeasurement('calibration:'.$measurement['server_addr'], $measurement);
    }
}
