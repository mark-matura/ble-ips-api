<?php

namespace App\Http\Controllers\Api\v1;
use Illuminate\Http\Request;

use App\Room;

class RoomController extends Controller
{
    public function __construct()
    {
    }

    /**
    * @param Request $request
    *
    * @return \Illuminate\Http\Response
    */

    public function get(Request $request) {
        return Room::first();
    }
}
