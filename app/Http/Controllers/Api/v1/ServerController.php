<?php

namespace App\Http\Controllers\Api\v1;
use Illuminate\Http\Request;

use App\Server;
use App\Helpers\CacheInterface;
use App\Helpers\FitCalibrationData;

use Illuminate\Support\Facades\DB;
use Log;

class ServerController extends Controller
{
    /**
    * @param Request $request
    *
    * Return the requesting user info
    *
    * @return \Illuminate\Http\Response
    */

    public function getAll(Request $request) {
        return Server::all();
    }

    /**
    * @param Request $request
    *
    * get the server by its id
    *
    * @return \Illuminate\Http\Response
    */
    public function getById(Request $request) {
        $this->validate($request, [
            'id' => 'required|integer|exists:App\Server,id',
        ]);

        $query = Server::where('id', $request->id)->with('measurements');

        if($this->validationHelper->ifNotExists($query)) {
            return response('Resource with specified identifier was not found');
        }

        return $query->first();
    }

    /**
    * @param Request $request
    *
    * store a complete new server manually with all param
    *
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request) {
        $this->validate($request, [
            'mac_addr' => 'required|string',
        ]);

        log::info("Storing new server with mac ").$request->mac_addr;

        // set server in RDBMS
        if (!Server::where("mac_addr", $request->mac_addr)->exists()) {
            log::info('Server does not exist yet');
            $server = new Server;
            $server->mac_addr = $request->mac_addr;

            // some default data based on info from a calibration walk
            $server->signal_propagation_exp = 2.2421283703413;
            $server->reference_path_loss = -47.899677614236;

            $server->save();
            return response('Successfully saved the server', 200);
        }
        return response('Server already exists', 200);
    }

    /**
    * @param Request $request
    *
    * Calibrate the environment's path loss model constants using sample measurements at known distances
    *
    * @return \Illuminate\Http\Response
    */
    public function calibrate(Request $request) {
        $this->validate($request, [
            'mac_addr' => 'required|string',
        ]);

        $server = Server::where("mac_addr", $request->mac_addr)->first();

        $measurements = CacheInterface::retrieveMeasurements('calibration:'.$server->mac_addr);

        $fitCalibrationData = new FitCalibrationData($measurements);

        $server->signal_propagation_exp = $fitCalibrationData->getNFit();
        $server->reference_path_loss = $fitCalibrationData->getAFit();

        $server->save();

        return response('Successfully calibrated the server coefficients');
    }

    /**
    * @param Request $request
    *
    * Update an existing server
    *
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request) {
        $this->validate($request, [
            'id' => 'required|integer|exists:App\Server,id',
            'mac_addr' => 'required|string|',
            'active' => 'required|boolean',
            'color' => 'required|string',
            'signal_propagation_exp' => 'required|numeric',
            'reference_path_loss' => 'required|numeric',
            'x_coord' => 'required|numeric',
            'y_coord' => 'required|numeric',
        ]);

        $server = Server::where("id", $request->id)->first();
        $server->mac_addr = $request->mac_addr;
        $server->active = $request->active;
        $server->color = $request->color;

        $server->signal_propagation_exp = $request->signal_propagation_exp;
        $server->reference_path_loss = $request->reference_path_loss;

        $server->x_coord = $request->x_coord;
        $server->y_coord = $request->y_coord;
        $server->save();

        return Response('Successfully updated the server with MAC address "'.$server->mac_addr.'"', 200);
    }

    /**
    * @param Request $request
    *
    * Remove an existing server and its associated measurements
    *
    * @return \Illuminate\Http\Response
    */
    public function remove(Request $request) {
        $this->validate($request, [
            'id' => 'required|integer|exists:App\Server,id',
        ]);

        $server = $this->getById($request);
        $server->delete();

        return Response('Successfully removed the server with MAC address "'.$server->mac_addr.'"', 200);
    }
}