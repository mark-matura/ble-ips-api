<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $fillable = [
        'x_coord',
        'y_coord',
    ];

    /**
     * The attributes that should be cast to native types. Sometimes Lumen returns booleans as integer 1 or 0 which is annoying
     * https://laravel.com/docs/6.x/eloquent-mutators#attribute-casting
     *
     * @var array
     */
    protected $casts = [
        'x_coord' => 'float',
        'y_coord' => 'float',
    ];

    /**
     * https://laravel.com/docs/7.x/eloquent-relationships#one-to-many
     *
     * A position belongs to a client
     */
    public function client()
    {
        return $this->belongsTo('App\Client');
    }
}
