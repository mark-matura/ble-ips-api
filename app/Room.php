<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = [
        'name',
        'x_length',
        'y_length',
    ];

    /**
     * The attributes that should be cast to native types. Sometimes Lumen returns booleans as integer 1 or 0 which is annoying
     * https://laravel.com/docs/6.x/eloquent-mutators#attribute-casting
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'x_length' => 'float',
        'y_length' => 'float',
    ];

    /**
     * https://laravel.com/docs/7.x/eloquent-relationships#one-to-many
     *
     * A position belongs to a client
     */
    public function servers()
    {
        return $this->hasMany('App\Server');
    }
}
