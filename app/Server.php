<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    protected $fillable = [
        'mac_addr',
        'active',
        'color',
        'reference_path_loss',
        'signal_propagation_exp',
        'x_coord',
        'y_coord',
    ];

    /**
     * The attributes that should be cast to native types. Sometimes Lumen returns booleans as integer 1 or 0 which is annoying
     * https://laravel.com/docs/7.x/eloquent-mutators#attribute-casting
     *
     * @var array
     */
    protected $casts = [
        'mac_addr' => 'string',
        'active' => 'boolean',
        'color' => 'string',
        'reference_path_loss' => 'float',
        'signal_propagation_exp' => 'float',
        'x_coord' => 'float',
        'y_coord' => 'float',
    ];
}
