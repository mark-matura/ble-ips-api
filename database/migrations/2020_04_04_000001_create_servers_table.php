<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servers', function (Blueprint $table) {
            $table->increments('id'); // primary key
            $table->macAddress('mac_addr'); // unique identifier of bluetooth / wifi device
            $table->double('x_coord'); // location of server in room, maybe switch to double later on
            $table->double('y_coord');

            /*
                A environment is generally the room where the servers are located in. Like this
                The relation betwen servers and the environment links measurement data with the path
                loss model coefficients that are specific to the environment.
            */

            $table->integer('environment_id')->unsigned()->index()->nullable();
            $table->foreign('environment_id')->references('id')->on('environments');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servers');
    }
}
