<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    Save calculated positions in this table here. It is then possible to relate the position of the client
    to the timestamps in order to track objects.
*/

class CreatePositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('positions', function (Blueprint $table) {
            $table->increments('id'); // primary key

            $table->double('x_coord'); // location of client in room, maybe switch to double later on
            $table->double('y_coord');

            /*
                Coordinates possibly are unnecessary depending on how fast the program can calculate positions.
                Then a foreign key to clients and extendedly to their measurements is enough.
                To return a tuple of coordinates the program then recalculates the coords every time.
                This does not really efficiently permit location tracking over time though.
            */

            $table->integer('client_id')->unsigned()->index()->nullable();
            $table->foreign('client_id')->references('id')->on('clients');


            //associate the position with the environment in which the client is situated
            $table->integer('environment_id')->unsigned()->index()->nullable();
            $table->foreign('environment_id')->references('id')->on('environments');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('positions');
    }
}
