<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    Nomenclature is chosen to allow for additional sensor data to be included. The columns will
    ultimately be associated with measurements of the device sensor data.
*/

class CreateMeasurementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measurements', function (Blueprint $table) {
            $table->increments('id'); // primary key

            $table->double('rssi');

            // a measurement is related to a client
            $table->integer('client_id')->unsigned()->index()->nullable();
            $table->foreign('client_id')->references('id')->on('clients');

            //  and is also related to a server
            $table->integer('server_id')->unsigned()->index()->nullable();
            $table->foreign('server_id')->references('id')->on('servers');

            // save created at and updated at date automatically
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('measurements');
    }
}
