<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemovePlmInfoFromServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('servers', function($table) {
            $table->dropColumn('avg_gateway_rssi');
            $table->dropColumn('tx_power');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('servers', function (Blueprint $table) {
            $table->double('avg_gateway_rssi')->after('mac_addr')->default(-55.5);
            $table->integer('tx_power')->after('mac_addr')->default(3);
        });
    }
}
