<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveEnvironmentIdFromServers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('servers', function($table) {
            $table->dropForeign(['environment_id']);
            $table->dropColumn('environment_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('servers', function (Blueprint $table) {
                //associate the position with the environment in which the client is situated
            $table->integer('environment_id')->unsigned()->index()->nullable();
            $table->foreign('environment_id')->references('id')->on('environments');
        });
    }
}
