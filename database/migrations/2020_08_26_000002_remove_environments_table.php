<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveEnvironmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('environments');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('environments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100)->default('My Environment');

            $table->double('reference_path_loss')->after('name')->default(-47.899677614236);
            $table->double('signal_propagation_exp')->after('reference_path_loss')->default(2.2421283703413);

            $table->timestamps();
        });
    }
}
