<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPathLossModelConstantsToServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * Choice of default values are explained in blog post
     * @see https://blog.markmarolf.com/documentation/05-11-path-loss-constant/#least-squares-based-regression
     *
     * @return void
     */
    public function up()
    {
        Schema::table('servers', function($table) {
            $table->double('reference_path_loss')->after('mac_addr')->default(-47.899677614236);
            $table->double('signal_propagation_exp')->after('reference_path_loss')->default(2.2421283703413);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('servers', function (Blueprint $table) {
            $table->dropColumn('reference_path_loss');
            $table->dropColumn('signal_propagation_exp');
        });
    }
}
