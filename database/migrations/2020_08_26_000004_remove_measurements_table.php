<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveMeasurementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('measurements');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('measurements', function (Blueprint $table) {
            $table->increments('id'); // primary key

            $table->double('rssi');

            // a measurement is related to a client
            $table->integer('client_id')->unsigned()->index()->nullable();
            $table->foreign('client_id')->references('id')->on('clients');

            //  and is also related to a server
            $table->integer('server_id')->unsigned()->index()->nullable();
            $table->foreign('server_id')->references('id')->on('servers');

            // save created at and updated at date automatically
            $table->timestamps();
        });
    }
}
