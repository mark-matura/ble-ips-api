<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActiveFlagToServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * Choice of default values are explained in blog post
     * @see https://blog.markmarolf.com/documentation/05-11-path-loss-constant/#least-squares-based-regression
     *
     * @return void
     */
    public function up()
    {
        Schema::table('servers', function($table) {
            $table->boolean('active')->after('mac_addr')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('servers', function (Blueprint $table) {
            $table->dropColumn('active');
        });
    }
}
