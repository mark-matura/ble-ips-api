<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColorToClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * Choice of default values are explained in blog post
     * @see https://blog.markmarolf.com/documentation/05-11-path-loss-constant/#least-squares-based-regression
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function($table) {
            $table->string('color')->after('mac_addr')->default('#000000');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->dropColumn('color');
        });
    }
}
