<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActualCoordinatesToClientsTable extends Migration
{
    public function up()
    {
        Schema::table('clients', function($table) {
            $table->float('actual_x')->default(0.0)->after('mac_addr');
            $table->float('actual_y')->default(0.0)->after('actual_x');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->dropColumn('actual_x');
            $table->dropColumn('actual_y');
        });
    }
}
