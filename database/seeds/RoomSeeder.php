<?php

use Illuminate\Database\Seeder;
use App\Room;

class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $room = new Room;
        $room->name = "Living room";
        $room->x_length = 4.25;
        $room->y_length = 6.3;
        $room->save();
    }
}
