# https://docs.docker.com/compose/compose-file/#restart

version: "3.8"
services:
  api:
    tty: true
    build:
      args:
        user: ipsuser
        uid: 420
      context: ./
      dockerfile: Dockerfile.api
    image: ble-ips-api
    container_name: ${APP_NAME}-api
    restart: unless-stopped
    working_dir: /var/www/
    # colon binds pwd to /var/www in docker image
    # all files are synced across both filesystems
    volumes:
      - ./:/var/www
      - ./docker/php-fpm/php.ini-production:/usr/local/etc/php/php.ini-production
      - ./docker/php-fpm/www.conf:/usr/local/etc/php-fpm.d/www.conf
    networks:
      - ips-network
  db:
    image: linuxserver/mariadb
    container_name: ${APP_NAME}-db
    restart: unless-stopped
    # forward port 3306 in host to 3306 in container
    ports:
      - '3306:3306'
    environment:
      # compose automatically loads .env variables into docker image on build
      MYSQL_DATABASE: ${DB_DATABASE}
      MYSQL_ROOT_PASSWORD: ${DB_PASSWORD}
      MYSQL_PASSWORD: ${DB_PASSWORD}
      MYSQL_USER: ${DB_USERNAME}
      SERVICE_TAGS: dev
      SERVICE_NAME: mysql
    volumes:
      - ./docker/mysql:/docker-entrypoint-initdb.d

    networks:
      - ips-network
  phpmyadmin:
    depends_on:
      - db
    container_name: ${APP_NAME}-pma
    image: phpmyadmin/phpmyadmin
    restart: unless-stopped
    # pma is available via localhost:3000 from localhost
    ports:
      - '3000:80'
    environment:
      PMA_HOST: db
      MYSQL_ROOT_PASSWORD: password
    networks:
      - ips-network

  nginx:
    image: nginx:alpine
    container_name: ${APP_NAME}-nginx
    restart: unless-stopped
    depends_on:
      - api
    ports:
      - 8000:8000
    volumes:
      - ./:/var/www
      - ./docker/nginx:/etc/nginx/conf.d/
    networks:
      - ips-network

  redis:
    container_name: ${APP_NAME}-redis
    image: redis:alpine
    restart: unless-stopped
    volumes:
      - ./docker/redis:/var/lib/redis
      - ./docker/redis:/usr/local/etc/redis
    ports:
      - 6379:6379
    networks:
      - ips-network

# https://docs.docker.com/compose/reference/restart/
  trilaterator:
    tty: true
    container_name: ${APP_NAME}-trilaterator
    restart: on-failure
    env_file:
      - ".env"
    build:
      context: ./
      dockerfile: Dockerfile.python
    volumes:
      - ./:/var/www
    networks:
      - ips-network

# https://docs.docker.com/network/network-tutorial-standalone/
networks:
  ips-network:
    name: ${APP_NAME}-network
    driver: bridge
