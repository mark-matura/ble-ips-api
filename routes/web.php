<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function ($app) use($router) {
    $router->group(['namespace' => 'Api'], function() use ($router)
    {
        // Api v1
        $router->group(['prefix' => 'v1'], function ($app) use($router) {
            $router->group(['namespace' => 'v1'], function() use ($router)
            {
                $router->get('/', function () use ($router) {
                    return "api/v1";
                });

                $router->group(['prefix' => 'client'], function () use($router){
                    $router->get('','ClientController@getAll');
                    $router->get('getById','ClientController@getById');

                    $router->post('store','ClientController@store');
                    $router->post('update','ClientController@update');
                    $router->post('remove','ClientController@remove');
                });

                $router->group(['prefix' => 'server'], function () use($router){
                    $router->get('','ServerController@getAll');
                    $router->get('getById','ServerController@getById');

                    $router->post('store','ServerController@store');
                    $router->post('calibrate','ServerController@calibrate');
                    $router->post('update','ServerController@update');
                    $router->post('remove','ServerController@remove');
                });

                $router->group(['prefix' => 'measurement'], function () use($router){
                    $router->get('','MeasurementController@getAll');
                    $router->get('getById','MeasurementController@getById');
                    $router->get('getCalibrationData','MeasurementController@getCalibrationData');
                    $router->get('getForServerAndClient','MeasurementController@getForServerAndClient');

                    $router->post('store','MeasurementController@store');
                    $router->post('update','MeasurementController@update');
                    $router->post('remove','MeasurementController@remove');
                });

                $router->group(['prefix' => 'room'], function () use($router){
                    $router->get('','RoomController@get');
                });
            });
        });
    });
});
