<?php
use Tests\MeasurementFactory;

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

use App\Helpers\CacheInterface;

class CacheInterfaceTest extends TestCase {
    public function testStoringMeasurements() {
        //given three sample measurements
        $measurements = MeasurementFactory::getThreeMeasurements('Client');

        //when measurements are saved in cache
        foreach ($measurements as $measurememt) {
            Log::info('testing storing a measurement');
            CacheInterface::storeMeasurement('testingkey:clientmeasurements', $measurememt);
        }

        // then expect cache to contain three measurements
        $retrievedMeasurements = CacheInterface::retrieveMeasurements('testingkey:clientmeasurements', 3);

        $this->assertEquals(3, count($retrievedMeasurements));
        $this->assertTrue(count($retrievedMeasurements) !== 0);
    }

    /**
     * Clear cache after testing if Cache can be populated
     *
     * @depends testStoringMeasurements
     */
    public function testClearingCache() {
        // given there are three measurements in cache

        // when deleteKey is called,
        CacheInterface::deleteKey('testingkey:clientmeasurements');

        // expect cache to be cleared
        $retrievedMeasurements = CacheInterface::retrieveMeasurements('testingkey:clientmeasurements');
        $this->assertEquals(0, count($retrievedMeasurements));

    }

    /**
     * @depends testStoringMeasurements
     * @depends testClearingCache
     */
    public function testRetrievingAllMeasurements() {
        //given empty cache
        CacheInterface::deleteKey('testingkey:clientmeasurements');
        $measurements = MeasurementFactory::getThreeMeasurements('Client');

        //when measurements are saved in cache
        foreach ($measurements as $measurememt) {
            CacheInterface::storeMeasurement('testingkey:clientmeasurements', $measurememt);
            CacheInterface::storeMeasurement('testingkey:clientmeasurements', $measurememt);
        }

        // then retrieving measurements should give us 2*3 = 6 measurements
        $retrievedMeasurements = CacheInterface::retrieveMeasurements('testingkey:clientmeasurements');
        $this->assertEquals(6, count($retrievedMeasurements));
    }

    /**
     * @depends testRetrievingAllMeasurements
     */
    public function testFlushingCache() {
        // given 50 measurements are in cache
        $measurements = MeasurementFactory::getRandomMeasurements(50, 'calibration');
        foreach ($measurements as $measurement) {
            $measurement = (array)$measurement;
            CacheInterface::storeMeasurement('testingkey:clientmeasurements', $measurement);
        }

        // when
        CacheInterface::flushAll();

        $results = CacheInterface::retrieveMeasurements('testingkey:clientmeasurements');
        //then
        $this->assertEquals(0, count($results));
    }
}