<?php
use Tests\MeasurementFactory;
use App\Helpers\CalculationHelper;

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class CalculationHelperTest extends TestCase {
    public function testGettingLogScaledAverageRssi() {
        // given an array of measurements supposedly from a calibration
        $measurements = MeasurementFactory::getThreeMeasurements('calibration');

        // when put into the calibration fitter
        $result = CalculationHelper::getLogScaledAverageRssi($measurements);

        // then
        $this->assertEquals(round(-52.0194368, 3), round($result, 3));
    }

    public function testGettingIndexOfMeasurement() {
        // given an array of measurements and a known index
        $measurements = MeasurementFactory::getRandomMeasurements(50, 'client');
        $predefinedIndex = rand(0, 50);

        // when
        $measurement = (object) $measurements[$predefinedIndex];
        $calculatedIndex = CalculationHelper::getIndexOfMeasurement($measurement, $measurements);

        Log::info('predefinedIndex'. $predefinedIndex);
        Log::info('calculatedIndex'. $calculatedIndex);

        $this->assertEquals($predefinedIndex, $calculatedIndex);
    }
}