<?php
use Tests\MeasurementFactory;

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ClientAPITest extends TestCase {
    private $name = 'TestClient';
    private $mac_addr = 'b8:27:eb:b2:5f:40';

    public function testHandlingClientJsonPayload() {
        // when
        $response = $this->call('POST', '/api/v1/client/store', ['name' => $this->name, 'mac_addr' => $this->mac_addr]);

        // then
        $this->assertEquals(200, $response->status());
    }

    /**
     * @depends testHandlingClientJsonPayload
     */
    public function testStoringClientInDatabase() {
        // given post request done on client/store endpoint then
        $this->seeInDatabase('clients', ['name' => $this->name]);
        $this->seeInDatabase('clients', ['mac_addr' => $this->mac_addr]);
    }

    /**
     * @depends testHandlingClientJsonPayload
     */
    public function testStoringExistingClientInDatabase() {
        // given client alredy in database
        $this->seeInDatabase('clients', ['name' => $this->name]);
        $this->seeInDatabase('clients', ['mac_addr' => $this->mac_addr]);

        // when posting same client again
        $response = $this->call('POST', '/api/v1/client/store', ['name' => $this->name, 'mac_addr' => $this->mac_addr]);
        // then
        $this->assertEquals(200, $response->status());
        $this->assertEquals('Client already exists', $response->content());
    }
}