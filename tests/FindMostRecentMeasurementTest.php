<?php
use Tests\MeasurementFactory;

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

use App\Helpers\FindMostRecentMeasurement;

class FindMostRecentMeasurementTest extends TestCase {
    public function testFindingMostRecentMeasurement() {
        // given some random measurements and one randomly defined as the target
        $randomGatewayMeasurements = MeasurementFactory::getRandomMeasurements(50, 'gateway');
        $targetClientMeasurement = (object) $randomGatewayMeasurements[rand(10, 40)];
        // when searching for most recent gateway measurement relative to client target
        $mostRecent = new FindMostRecentMeasurement($targetClientMeasurement, $randomGatewayMeasurements);

        // then target should be equal to the most recent since it is copy with time delta 0
        $this->assertEquals($targetClientMeasurement, $mostRecent->get());
    }

    public function testSubmittingNoMeasurements() {
        $this->expectException(Exception::class);

        // given an empty search array and a target
        $target = (object) MeasurementFactory::getRandomMeasurements(1, 'client');
        $emptyArray = array();

        $mostRecent = new FindMostRecentMeasurement($target, $emptyArray);
    }
}