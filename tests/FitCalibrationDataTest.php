<?php
use Tests\MeasurementFactory;
use App\Helpers\FitCalibrationData;

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class FitCalibrationDataTest extends TestCase {
    public function testFittingCalibrationData() {
        log::info(":::::::::::::::::::: testing fit calibration ::::::::::::::::::::::::::");

        // given an array of measurements supposedly from a calibration
        $measurements = MeasurementFactory::getThreeMeasurements('calibration');
        $expectedN = round(1.459, 2);
        $expectedA = round(-54.14, 2);

        // when put into the calibration fitter
        $calibrationFit = new FitCalibrationData($measurements);

        // then
        $this->assertEquals($expectedN, round($calibrationFit->getNFit(), 2));
        $this->assertEquals($expectedA, round($calibrationFit->getAFit(), 2));
    }

    public function testSupplyingNoMeasurements() {
        $this->expectException(Exception::class);
        $calibrationFit = new FitCalibrationData(array());
    }

}
?>
