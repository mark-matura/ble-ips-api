<?php
use Tests\MeasurementFactory;

use App\Helpers\CacheInterface;

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

use Illuminate\Support\Facades\Redis;

class MeasurementAPITest extends TestCase {
    public function testStoringMeasurementAPI() {
        $this->withoutEvents();

        // given some storable measurements
        $measurements = MeasurementFactory::getCompletePayloadWithThreeFakeMeasurements('client');

        // when endpoint is called with measurements as array
        $response = $this->call('POST', '/api/v1/measurement/store', $measurements);

        //then response should be successful
        $this->assertEquals(200, $response->status());
        $this->assertEquals('Successfully stored the measurements', $response->content());
    }

    /**
     * @depends testStoringMeasurementAPI
     */
    public function testCachingClientMeasurements() {

        // given an empty cache and a fake request payload
        CacheInterface::flushAll();
        $requestPayload = MeasurementFactory::getCompletePayloadWithThreeFakeMeasurements('client');

        // when the three fake measurements are handled by API
        $this->call('POST', '/api/v1/measurement/store', $requestPayload);

        // then
        $key = 'server:'.$requestPayload['measurements'][0]['server_addr'].':client:'.$requestPayload['client_addr'].':measurements';
        $cachedMeasurements = CacheInterface::retrieveMeasurements($key);

        log::info(count($cachedMeasurements));
        $this->assertTrue(count($cachedMeasurements) === 3);
    }

    /**
     * @depends testStoringMeasurementAPI
     */
    public function testCachingGatewayMeasurements() {
        // given an empty cache and a fake request payload
        CacheInterface::flushAll();
        $requestPayload = MeasurementFactory::getCompletePayloadWithThreeFakeMeasurements('gateway');

        // when the three fake measurements are handled by API
        $this->call('POST', '/api/v1/measurement/store', $requestPayload);

        // then expect gateway measurements to be in cache
        $key = 'gateway:server:'.$requestPayload['measurements'][0]['server_addr'];
        $cachedMeasurements = CacheInterface::retrieveMeasurements($key);

        $this->assertTrue(count($cachedMeasurements) === 3);
    }

    /**
     * @depends testStoringMeasurementAPI
     */
    public function testCachingCalibrationMeasurements() {
        // given an empty cache and a fake request payload
        CacheInterface::flushAll();
        $requestPayload = MeasurementFactory::getCompletePayloadWithThreeFakeMeasurements('calibration');

        // when the three fake measurements are handled by API
        $this->call('POST', '/api/v1/measurement/store', $requestPayload);

        // then expect gateway measurements to be in cache
        $cachedMeasurements = CacheInterface::retrieveMeasurements('calibration:'.$requestPayload['measurements'][0]['server_addr']);

        $this->assertTrue(count($cachedMeasurements) === 3);
    }
}