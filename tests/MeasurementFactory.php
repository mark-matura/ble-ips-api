<?php
namespace Tests;

class MeasurementFactory {
    public static function getMeasurementsWithPredefinedRssi(array $rssiValues, string $origin): array {
        $returnarray = array();
        foreach ($rssiValues as $rssiValue) {
            $time = (int)round(microtime(true) * 1000) + rand(0, 100000); // time in milliseconds plus a random number to simulate time diversity
            $rssi = $rssiValue;

            $measurement = array(
                'rssi' => $rssi,
                'server_addr' => '24:6f:28:7a:57:02',
                'origin' => $origin,
                'timestamp' => $time,
            );

            $measurement = (object) $measurement;
            array_push($returnarray, $measurement);
        }

        return $returnarray;
    }

    public static function getRandomMeasurements(int $measurementCount, string $origin): array {
        $returnarray = array();

        for ($index=0; $index < $measurementCount; $index++) {
            $time = (int)round(microtime(true) * 1000) + rand(0, 100000); // time in milliseconds plus a random number to simulate time diversity
            $rssi = -57 + rand (-15, 15 );

            $measurement = array(
                'rssi' => $rssi,
                'server_addr' => '24:6f:28:7a:57:02',
                'origin' => $origin,
                'timestamp' => $time,
            );

            $measurement = (object) $measurement;
            array_push($returnarray, $measurement);
        }

        return $returnarray;
    }

    /**
     * Get an array of fake measurements with a fake client mac address
     * @param string $origin origin of the measurements
     */
    public static function getThreeMeasurements(string $origin): array {
        return array (
            0 =>
            array (
                'rssi' => -48,
                'server_addr' => '24:6f:28:7a:57:02',
                'origin' => $origin,
                'distance' => 0.5,
                'timestamp' => 50,
            ),
            1 =>
            array (
                'rssi' => -57,
                'server_addr' => '24:6f:28:7a:57:02',
                'origin' => $origin,
                'distance' => 1,
                'timestamp' => 500,
            ),
            2 =>
            array (
                'rssi' => -60,
                'server_addr' => '24:6f:28:7a:57:02',
                'origin' => $origin,
                'distance' => 3,
                'timestamp' => 1000,
            )
        );
    }

    /**
     * Get an array of fake measurements with a fake client mac address
     * @param string $origin of where the measurements came from
     */
    public static function getCompletePayloadWithThreeFakeMeasurements(string $origin) {
        $measurementRequest = [
            'client_addr' => 'b8:27:eb:b2:5f:40',
            'measurements' => array (
                0 =>
                array (
                    'rssi' => -55,
                    'server_addr' => '24:6f:28:7a:57:02',
                    'origin' => $origin,
                    'timestamp' => 110.25,
                ),
                1 =>
                array (
                    'rssi' => -52,
                    'server_addr' => '24:6f:28:7a:57:02',
                    'origin' => $origin,
                    'timestamp' => 110.25,
                ),
                2 =>
                array (
                    'rssi' => -59,
                    'server_addr' => '24:6f:28:7a:57:02',
                    'origin' => $origin,
                    'timestamp' => 110.25,
                ),
            )
        ];

        return $measurementRequest;
    }
}
?>