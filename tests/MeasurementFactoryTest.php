

<?php
use Tests\MeasurementFactory;

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class MeasurementFactoryTest extends TestCase {
    public function testGettingMeasurementsFromFactory() {
        //given three sample measurements
        $measurements = MeasurementFactory::getThreeMeasurements('Client');
        // then
        $this->assertEquals(3, count($measurements));
    }
}