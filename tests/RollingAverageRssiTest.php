<?php
use Tests\MeasurementFactory;

use App\Helpers\CalculationHelper;
use App\Helpers\RollingAverageRssi;
use App\Helpers\FitCalibrationData;

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class RollingAverageRssiTest extends TestCase {

    public function testGettingRollingAverageRssi() {

        // given an array of measurements and a central value to be smoothed
        $rssiValues = array(-56, -55, -43, -59, -59, -61, -56, -44, -42, -43, -43, -59, -56, -56);
        $measurements = MeasurementFactory::getMeasurementsWithPredefinedRssi($rssiValues, 'gateway');
        $measurement = (object) $measurements[7]; //-44

        // when I put it through the rolling average class
        $rollingAverage = new RollingAverageRssi($measurement, $measurements);
        $result = $rollingAverage->getResult();

        // then expect the result to be the same as the average of a window determined by hand
        $manuallySliced = MeasurementFactory::getMeasurementsWithPredefinedRssi(array(-43, -59, -59, -61, -56, -44, -42, -43, -43, -59, -56), 'gateway');
        $this->assertEquals(CalculationHelper::getLogScaledAverageRssi($manuallySliced), $result);
    }
}