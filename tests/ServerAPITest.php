<?php
use Tests\MeasurementFactory;

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ServerAPITest extends TestCase {

    private $mac_addr = 'b8:27:eb:b2:5f:40';

    public function testHandlingServerAPI() {
        // when
        $response = $this->call('POST', '/api/v1/server/store', ['mac_addr' => $this->mac_addr]);

        // then
        $this->assertEquals(200, $response->status());
    }

    /**
     * @depends testHandlingServerAPI
     */
    public function testStoringServerInDatabase() {
        // given post request done on client/store endpoint then
        $this->seeInDatabase('servers', ['mac_addr' => $this->mac_addr]);
    }
}