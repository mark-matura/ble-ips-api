from models.server import Server
from models.client import Client

import mysql.connector
import datetime

from dotenv import load_dotenv

import os


class DatabaseConnector:
    def __connectToRDBMS():
        load_dotenv()

        mydb = mysql.connector.connect(
            host=os.getenv("DB_HOST"),
            user=os.getenv("DB_USERNAME"),
            database=os.getenv("DB_DATABASE"),
            password=os.getenv("DB_PASSWORD"),
            charset='utf8'
        )

        return mydb

    @staticmethod
    def query(query: str):
        db = DatabaseConnector.__connectToRDBMS()

        cursor = db.cursor()
        cursor.execute(query)
        return cursor.fetchall()

    @staticmethod
    def insert(positionCoordinates, client: Client):
        print(str(round(positionCoordinates[0], 2)) +
              '\t' + str(round(positionCoordinates[1], 2)))

        # https://www.w3schools.com/python/python_mysql_insert.asp
        db = DatabaseConnector.__connectToRDBMS()

        cursor = db.cursor()

        created_at = datetime.datetime.utcnow().isoformat()
        sql = "INSERT INTO `positions`(`x_coord`, `y_coord`, `client_id`, `created_at`, `updated_at`) VALUES (%s, %s, %s, %s, %s)"

        values = (
            positionCoordinates[0], positionCoordinates[1], client.id, created_at, created_at)
        cursor.execute(sql, values)

        db.commit()
