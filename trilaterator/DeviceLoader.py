from models.server import Server
from models.client import Client

from DatabaseConnector import DatabaseConnector


class DeviceLoader:
    def getClients(self):
        clients = []
        queryResults = DatabaseConnector.query(
            "SELECT id, name, mac_addr FROM clients")

        if(queryResults):
            for (id, name, mac_addr) in queryResults:
                client = Client(id, name, mac_addr)
                clients.append(client)
            return clients

        raise Exception("Could not find any clients")

    def getServers(self):
        servers = []
        queryResults = DatabaseConnector.query(
            "SELECT mac_addr, reference_path_loss, signal_propagation_exp, x_coord, y_coord FROM servers WHERE active = 1")

        if(queryResults):
            for (mac_addr, reference_path_loss, signal_propagation_exp, x_coord, y_coord) in queryResults:
                server = Server(mac_addr, reference_path_loss,
                                signal_propagation_exp, x_coord, y_coord)
                servers.append(server)
            return servers

        raise Exception("Could not find any servers")
