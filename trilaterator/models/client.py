class Client:
    def __init__(self, id, name, mac_addr):
        self.id: int = int(id)
        self.name: str = str(name)
        self.mac_addr: str = str(mac_addr)
