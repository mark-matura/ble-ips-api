import json


class Measurement(object):
    def __init__(self, encodedJsonReading):
        self.properties: dict = json.loads(encodedJsonReading)
        self.__bindPropsToObject()

    def __bindPropsToObject(self):
        self.rssi: float = self.properties['rssi']
        self.server_addr: str = self.properties['server_addr']
        self.origin: str = self.properties['origin']
        self.timestamp: int = self.properties['timestamp']
