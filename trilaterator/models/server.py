from models.measurement import Measurement

from typing import List


class Server:
    def __init__(self, mac_addr, reference_path_loss, signal_propagation_exp, x_coord, y_coord):
        self.mac_addr: str = str(mac_addr)

        self.reference_path_loss: float = float(reference_path_loss)
        self.signal_propagation_exp: float = float(signal_propagation_exp)
        self.x_coord: float = float(x_coord)
        self.y_coord: float = float(y_coord)

        self.measurements: List[Measurement] = []

    def mostRecentMeasurement(self) -> Measurement:
        return max(self.measurements, key=lambda measurement: measurement.timestamp)
