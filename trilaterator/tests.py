from random import Random
from trilaterate import *
from models.server import Server
from models.measurement import Measurement

from typing import List
import unittest
import random
import collections


class TestAveragingFunctions(unittest.TestCase):
    def test_converting_dbm_to_mWatt(self):
        # given a rssi value of -45
        rssi: int = -45

        # when converting to mWatt
        res: float = round(convertToMWatt(rssi), 12)

        self.assertEqual(res, round(0.000031622776602, 12))

    def test_converting_mWatt_to_dbm(self):
        # given mWatt of 0.000065655656565
        mwatt = 0.000065655656565

        # when converting to dbm
        res = round(convertToDBm(mwatt), 2)

        self.assertEqual(res, round(-41.827278513, 2))

    def test_getting_log_scaled_average_rssi(self):
        # given a list of three measurements

        measurements = GetMockObjects.measurements

        # when getting avg
        res = round(logScaledAvgRssi(measurements), 5)

        self.assertEqual(res, round(-31.98269, 5))


class TestGettingRollingAverageRssi(unittest.TestCase):
    def test_getting_measurement_closest_to_time(self):
        # given a time in milliseconds and some measurements
        arbitraryTime = 1598525880000 - random.randint(-4999, 4999)
        measurements = GetMockObjects.measurements

        # when getting measurement from one second ago
        measurement = getMeasurementClosestToTime(arbitraryTime, measurements)

        self.assertEqual(measurement, measurements[0])

    def test_getting_rolling_average_around_measurement(self):
        # given some measurements and a central measurement
        measurements: Measurement = GetMockObjects.measurements  # lenghth is 8
        index = 3

        # when getting the rolling average around this measurement
        avg = round(rollingAverage(index, measurements, 3), 5)
        self.assertEqual(avg, round(-31.403039341, 5))


class TestServerClass(unittest.TestCase):
    def test_getting_most_recent(self):
        # given a server with measurements
        server = Server('24:6f:28:7a:57:02', -42.992269109053,
                        2.0676734826131, 0, 3.98)
        server.measurements = GetMockObjects.measurements

        # when getting the most recent measurement
        mostRecent = server.mostRecentMeasurement()

        # expect the timestamp to eq the latest measurement in list
        self.assertEqual(mostRecent.timestamp, 1598525894000)


class TestPathLoss(unittest.TestCase):
    def test_getting_distance(self):
        # given a server with measurements
        server: Server = Server('24:6f:28:7a:57:02', -42,
                                2, 0, 3.98)
        server.measurements = GetMockObjects.measurements

        # when getting the distance of server based on closest measurement
        # attached to the server object. In this case the RSSI = -65 dBm
        distance: float = round(getDistance(server), 4)
        # expect the distance value to eq manually calculated distance
        self.assertEqual(distance, 1.9115)


# class TestTrilateration(unittest.TestCase):
#     def test_getting_nearest_three_servers(self):
#         # given a list of servers with same coefficients
#         servers = GetMockObjects.serversWithSameCoefficients

#         # when the measurements for first three in list are set to random number between -20 and -30
#         for server in servers[0:3]:
#             server.measurements = [
#                 Measurement(
#                     "{\"rssi\":" + str(random.randint(-30, -20)) + ",\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525860000}"),
#                 Measurement(
#                     "{\"rssi\":" + str(random.randint(-30, -20)) + ",\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525860000}"),
#                 Measurement(
#                     "{\"rssi\":" + str(random.randint(-30, -20)) + ",\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525860000}"),
#                 Measurement(
#                     "{\"rssi\":" + str(random.randint(-30, -20)) + ",\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525860000}"),
#                 Measurement(
#                     "{\"rssi\":" + str(random.randint(-30, -20)) + ",\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525860000}"),
#                 Measurement(
#                     "{\"rssi\":" + str(random.randint(-30, -20)) + ",\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525860000}"),
#                 Measurement(
#                     "{\"rssi\":" + str(random.randint(-30, -20)) + ",\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525860000}"),
#                 Measurement(
#                     "{\"rssi\":" + str(random.randint(-30, -20)) + ",\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525860000}"),
#                 Measurement(
#                     "{\"rssi\":" + str(random.randint(-30, -20)) + ",\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525860000}"),
#                 Measurement(
#                     "{\"rssi\":" + str(random.randint(-30, -20)) + ",\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525860000}"),
#             ]

#         # and the other servers are artificially really far away according to PLM
#         for server in servers[3:]:
#             server.measurements = [
#                 Measurement("{\"rssi\":" + str(random.randint(-90, -60)) +
#                             ",\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525860000}"),
#                 Measurement("{\"rssi\":" + str(random.randint(-90, -60)) +
#                             ",\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525860000}"),
#                 Measurement("{\"rssi\":" + str(random.randint(-90, -60)) +
#                             ",\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525860000}"),
#                 Measurement("{\"rssi\":" + str(random.randint(-90, -60)) +
#                             ",\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525860000}"),
#                 Measurement("{\"rssi\":" + str(random.randint(-90, -60)) +
#                             ",\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525860000}"),
#                 Measurement("{\"rssi\":" + str(random.randint(-90, -60)) +
#                             ",\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525860000}"),
#                 Measurement("{\"rssi\":" + str(random.randint(-90, -60)) +
#                             ",\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525860000}"),
#                 Measurement("{\"rssi\":" + str(random.randint(-90, -60)) +
#                             ",\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525860000}"),
#                 Measurement("{\"rssi\":" + str(random.randint(-90, -60)) +
#                             ",\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525860000}"),
#                 Measurement("{\"rssi\":" + str(random.randint(-90, -60)) +
#                             ",\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525860000}"),
#             ]

#         closestThree = getClosestThreeServers(servers)

#         # Expect the elements in list of closest to be equal to slice of first three in servers (not sorted)
#         self.assertEqual(type(closestThree), list)
#         self.assertEqual(collections.Counter(closestThree),
#                          collections.Counter(servers[0:3]))

#     def test_calculating_residuals(self):
#         # given a list of servers with same coefficients
#         servers = GetMockObjects.serversWithSameCoefficients[0:3]

#         # when the measurements for first three in list are set to random number between -20 and -30
#         for server in servers:
#             server.measurements = [
#                 Measurement(
#                     "{\"rssi\":-45,\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525860000}"),
#                 Measurement(
#                     "{\"rssi\":-45,\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525870000}")
#             ]

#         # when getting the mean squared error with a guess of (1.5, 2) and aforementioned servers with RSSI = -45
#         mse = meanSquaredError((1.5, 2), servers)
#         mse = round(mse, 3)

#         self.assertEqual(mse, 42.081)


if __name__ == '__main__':
    unittest.main()


class GetMockObjects:
    # generate some random measurements
    measurements: List[Measurement] = [
        Measurement(
            "{\"rssi\":-60,\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525860000}"),
        Measurement(
            "{\"rssi\":-23,\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525870000}"),
        Measurement(
            "{\"rssi\":-65,\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525880000}"),
        Measurement(
            "{\"rssi\":-70,\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525890000}"),
        Measurement(
            "{\"rssi\":-58,\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525891000}"),
        Measurement(
            "{\"rssi\":-43,\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525892000}"),
        Measurement(
            "{\"rssi\":-56,\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525893000}"),
        Measurement(
            "{\"rssi\":-65,\"server_addr\":\"24:6f:28:7a:38:ae\",\"origin\":\"client\",\"timestamp\":1598525894000}")
    ]

    # mock servers
    servers: List[Server] = [
        Server('24:6f:28:7a:57:02', -42.992269109053,
               2.0676734826131, 0, 3.98),
        Server('24:6f:28:7a:48:2e', -39.036637566669,
               2.1663846355787, 0, 0),
        Server('24:6f:28:7a:54:16', -42.107757587865,
               2.2089422315674, 3.28, 1.99),
        Server('24:6f:28:7a:47:ee', -41.676658216241,
               2.2678087018855, 0, 1.99),
        Server('24:6f:28:7a:55:66', -42.482071981159,
               2.1877304995866, 1.64, 0),
        Server('24:6f:28:7a:38:ae', -46.345841281719,
               2.4984152813137, 1.64, 3.98),
        Server('24:6f:28:7a:45:8e', -34.444837281035,
               1.9326394931154, 3.28, 3.98),
        Server('24:6f:28:7a:41:3a', -40.887429242652,
               2.6059219558825, 3.28, 0)
    ]

    serversWithSameCoefficients: List[Server] = [
        Server('24:6f:28:7a:57:02', -42, 2, 0, 4),
        Server('24:6f:28:7a:48:2e', -42, 2, 0, 4),
        Server('24:6f:28:7a:54:16', -42, 2, 0, 4),
        Server('24:6f:28:7a:47:ee', -42, 2, 0, 1.99),
        Server('24:6f:28:7a:55:66', -42, 2, 1.64, 0),
        Server('24:6f:28:7a:38:ae', -42, 2, 1.64, 3.98),
        Server('24:6f:28:7a:45:8e', -42, 2, 3.28, 3.98),
        Server('24:6f:28:7a:41:3a', -42, 2, 3.28, 0)
    ]
