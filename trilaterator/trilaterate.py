import threading
import time
import math
import logging
import redis
import operator

from typing import List
from scipy.optimize import fmin_powell

from DeviceLoader import DeviceLoader
from DatabaseConnector import DatabaseConnector

from models.client import Client
from models.server import Server
from models.measurement import Measurement

servers: List[Server] = []
clients: List[Client] = []

currentTime: int = 0
deviceLoader = DeviceLoader()
redisClient = redis.Redis(host='redis', port=6379)

# change logging.INFO to ERROR to suppress messaging, INFO for verbose output
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')


def main() -> None:
    logging.debug(
        ":::::::::::::::::::::::::: MAIN THREAD CALLED::::::::::::::::::::::::::")

    threading.Timer(0.1, main).start()
    global currentTime
    currentTime = timeNow()

    refreshDeviceList()

    for client in clients:
        try:
            for server in servers:
                server.measurements = fetchMeasurementsFromRedis(
                    client, server)

            closestThreeServers: List[Server] = getClosestThreeServers(servers)

            # reduce mean squared error passing closest three servers as an argument
            positionCoordinates = fmin_powell(
                meanSquaredError, (1.5, 2), args=(closestThreeServers,), disp=False)

            DatabaseConnector.insert(positionCoordinates, client)
        except Exception as e:
            print(e)
            logging.exception(e)


def timeNow():
    """Get current time in milliseconds"""
    return int(round(time.time() * 1000))


def refreshDeviceList():
    global servers
    global clients

    servers = deviceLoader.getServers()
    clients = deviceLoader.getClients()


def fetchMeasurementsFromRedis(client: Client, server: Server) -> List[Server]:
    key = constructKey(client, server)
    measurements = []

    for m in (redisClient.lrange(key,  0, 199)):
        measurement = Measurement(m)
        measurements.append(measurement)

    if len(measurements) == 0:
        raise ValueError('no measurements were found for ' + key)

    logging.debug("fetched " + str(len(measurements)) +
                 " measurements from redis for key " + key)
    return measurements


def constructKey(client: Client, server: Server) -> str:
    return 'server:' + server.mac_addr + ':client:' + client.mac_addr + ':measurements'


def getClosestThreeServers(servers) -> List[Server]:
    if len(servers) < 3:
        raise ValueError(
            "Less than three servers supplied to getClosestThreeServers")

    # do the actual sorting
    sortedServers = sorted(
        servers, key=lambda server: getDistance(server),  reverse=False)

    for server in sortedServers[0:3]:
        logging.info(server.mac_addr + '\t' +
                     str(round(getDistance(server), 2)))

    return sortedServers[0:3]


def meanSquaredError(x, bestServers):
    """Return MSE between measured and computed distance.
    The goal is to optimize this in respect to X as a 1D array

    https://nrr.mit.edu/sites/default/files/documents/Lab_11_Localization.html#sec1b
    """
    meanSquaredError: float = 0.0

    for server in bestServers:
        squaredRadius: float = getDistance(server)**2

        xError = (x[0] - server.x_coord)**2
        yError = (x[1] - server.y_coord)**2

        meanSquaredError += (squaredRadius - xError - yError)**2
    return meanSquaredError


def getDistance(server: Server) -> float:
    """formula: (rssi - a_0) / -10 * n"""
    index = selectMeasurement(server)
    rollingAverageRssi = rollingAverage(index, server.measurements)

    logging.debug("Rolling average RSSI: " + str(rollingAverageRssi))

    exp: float = (rollingAverageRssi - server.reference_path_loss) / \
        (-10 * server.signal_propagation_exp)
    return 10**exp


def selectMeasurement(server: Server) -> int:
    index = getIndexOfMeasurementClosestToTime(
        (currentTime - 1000), server.measurements)
    if index is None:
        raise IndexError(
            "Could not find measurement one second before current time")

    return index


def getIndexOfMeasurementClosestToTime(time: int, measurements: List[Measurement]) -> int:
    if measurements is None or len(measurements) == 0:
        raise ValueError(
            "No measurements were supplied to getIndexOfMeasurementClosestToTime")

    deltas = [abs(measurement.timestamp - time)
              for measurement in measurements]
    min_index, min_delta = min(enumerate(deltas), key=operator.itemgetter(1))

    if min_index == 0:
        raise Exception("Could not find index of measurement")

    return min_index


def rollingAverage(index: int, measurements: List[Measurement], windowHalf: int = 5) -> float:
    logging.debug("index of measurement:\t" + str(index) +
                 " in \t" + str(len(measurements)) + " measurements")

    # + 1 is for the measurement at index $index, which also takes a space
    if (windowHalf + index) > len(measurements):
        raise ValueError("Rolling average: invalid index " + str(index) +
                         " for msmnt list of len " + str(len(measurements)) + ". Rolling average wont be able to find enough measurements.")

    leftWindowHalf = index - windowHalf
    rightWindowHalf = index + windowHalf + 1
    return logScaledAvgRssi(measurements[leftWindowHalf:rightWindowHalf])


def logScaledAvgRssi(measurements: List[Measurement]) -> float:
    if len(measurements) < 11:
        logging.warning("Only " + str(len(measurements)) +
                        " measurements supplied to logScaledAvgRssi")

    logScaledSum = sum(convertToMWatt(measurement.rssi)
                       for measurement in measurements)
    return convertToDBm(logScaledSum / len(measurements))


def convertToMWatt(rssi: int) -> float:
    return 10**(rssi / 10)


def convertToDBm(mWatt: float) -> float:
    return 10 * math.log10(mWatt)


# Only run if called explicitly
if __name__ == "__main__":
    main()
